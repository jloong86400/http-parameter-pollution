APP=secdim.lab.js

all: build

test:
	docker build --target build --build-arg TESTSPEC=usability --rm .

securitytest:
	docker build --target build --build-arg TESTSPEC=security --rm .

build:
	docker build --rm --tag=secdim.lab.js .

run:
	docker run -p 8080:8080 --rm secdim.lab.js

clean:
	docker image rm $(APP)
	docker system prune

.PHONY: all test securitytest clean
