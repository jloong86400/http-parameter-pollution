
function payment(action, amount) {
    if (amount === undefined){ return 'You must specify the amount' }
    if (action.withdraw) {
        return `Successfuly withdrew $${amount.amount}`;
    } else if (action.transfer) {
        return `Successfully transfered $${amount.amount}`;
    } 
    return 'You must specify an action: withdraw or transfer';
}

module.exports = payment;
