class Action {
    constructor(value) {
        this.withdraw = false;
        this.transfer = false;

        if (value === 'withdraw') {
            this.withdraw = true
        }
        else if (value === 'transfer') {
            this.transfer = true
        }
        else {
            throw new Error('Invalid action');
        }
    }
}
module.exports = { Action }
