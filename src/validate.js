function isValidAmount(value) {
    return toString.call(value) === '[object Number]' && value > 0 && value <= Number.MAX_VALUE;
}

module.exports = isValidAmount;
