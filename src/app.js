'use strict';

// requirements
const express = require('express');
const payment = require('./payment');
const { Action } = require('./action');
const { Amount } = require('./amount');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    // We only allow transfer
    try {
        const action = new Action(req.query.action);
        const amount = new Amount(req.query.amount);
    
        if (action.transfer) {
            res.send(payment(action,amount))
            return;
        }
    }
    catch {
        res.status(400).send('You can only transfer an amount');
    }
    res.status(400).send('You can only transfer an amount');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
