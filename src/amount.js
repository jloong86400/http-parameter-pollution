const isValidAmount = require('./validate');

class Amount {
    constructor(value) {
        if (!value || (value.length && value.length > 10)) {
            throw new RangeError("invalid input")
        }

        const num = Number(value);
        
        if(Number.isNaN(value) || num < 0 || num > 214783647) {
            throw new RangeError("invalid input");
        }
        if (isValidAmount(value)) {
            throw new RangeError("invalid input");
        }
        this.amount = value;
    }
}
module.exports = {Amount}